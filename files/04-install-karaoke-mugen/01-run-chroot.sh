#!/bin/bash -e

# versions

export DEBIAN_FRONTEND=noninteractive
MUGEN_VERSION=master
YARN_VERSION=1.9.4 # don't change that : https://github.com/yarnpkg/yarn/pull/6302
MUGEN_BASE_DIR=/opt/karaokemugen

# install nodejs and yarn

echo "insecure" > ~/.curlrc
curl -sSL https://deb.nodesource.com/setup_12.x | bash -
apt-get -y install nodejs
npm install -g --unsafe-perm yarn@${YARN_VERSION}

# https://www.raspberrypi.org/documentation/configuration/wireless/access-point.md

sed -i '/# KaraokeMugen/,$d' /etc/dhcpcd.conf
echo "# KaraokeMugen
interface wlan0
    static ip_address=192.168.4.1/24
    nohook wpa_supplicant" >> /etc/dhcpcd.conf

sed -i '/# KaraokeMugen/,$d' /etc/dnsmasq.conf
echo "# KaraokeMugen
interface=wlan0
dhcp-range=192.168.4.2,192.168.4.20,255.255.255.0,24h
address=/mugen.pi/192.168.4.1" >> /etc/dnsmasq.conf

echo "interface=wlan0
driver=nl80211
ssid=KaraokeMugen
hw_mode=g
channel=7
wmm_enabled=0
macaddr_acl=0
auth_algs=1
ignore_broadcast_ssid=0
wpa=2
wpa_passphrase=karaokemugen
wpa_key_mgmt=WPA-PSK
wpa_pairwise=TKIP
rsn_pairwise=CCMP" > /etc/hostapd/hostapd.conf
sed -i 's/#DAEMON_CONF=""/DAEMON_CONF="\/etc\/hostapd\/hostapd.conf"/' /etc/default/hostapd

sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf
echo "*filter
:INPUT ACCEPT [0:0]
:FORWARD ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
COMMIT
*nat
:PREROUTING ACCEPT [0:0]
:INPUT ACCEPT [0:0]
:OUTPUT ACCEPT [0:0]
:POSTROUTING ACCEPT [0:0]
-A PREROUTING -d 192.168.4.1/32 -p tcp -m tcp --dport 80 -j REDIRECT --to-ports 1337
-A POSTROUTING -o eth0 -j MASQUERADE
COMMIT" > /etc/iptables/rules.v4

# Karaoke Mugen service

echo "[Unit]
Description=Karaoke Mugen
After=network-online.target

[Service]
Type=simple

ExecStartPre=/bin/systemctl restart postgresql
ExecStart=/usr/bin/sudo -u pi yarn start
WorkingDirectory=${MUGEN_BASE_DIR}
Restart=on-failure
Environment=\"DISPLAY=:0\"

# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=30

[Install]
WantedBy=graphical.target" > /etc/systemd/system/karaokemugen.service

systemctl enable hostapd

# init Karaoke Mugen Password script

echo "cd ${MUGEN_BASE_DIR}
service postgresql restart
yarn start --sql --generate --forceAdminPassword karaokemugen
systemctl enable karaokemugen
service karaokemugen start
rm /boot/rc.local.txt" > /boot/rc.local.txt

# set default wallpaper

sed -i '/feh/d' /etc/xdg/openbox/autostart
echo "feh --bg-scale --no-fehbg /etc/xdg/openbox/bg.jpg" >> /etc/xdg/openbox/autostart

# install yarn and app from sources

git clone https://lab.shelter.moe/karaokemugen/karaokemugen-app.git ${MUGEN_BASE_DIR} || true
cd ${MUGEN_BASE_DIR}
git fetch -t
git checkout -f ${MUGEN_VERSION}
git pull || true
yarn setup

# cleaning

yarn cache clean
npm cache clean --force
apt-get -y clean

# generate the database and set the default password

service postgresql start
cp -f ${MUGEN_BASE_DIR}/database.sample.json ${MUGEN_BASE_DIR}/database.json
sed -i 's/"bundledPostgresBinary": true/"bundledPostgresBinary": false/' ${MUGEN_BASE_DIR}/database.json
sed -i 's/"port": 6559/"port": 5432/' ${MUGEN_BASE_DIR}/database.json
sudo -u postgres psql -c "DROP DATABASE IF EXISTS karaokemugen_app;"
sudo -u postgres psql -c "CREATE DATABASE karaokemugen_app ENCODING 'UTF8';"
sudo -u postgres psql -c "DROP USER IF EXISTS karaokemugen_app;"
sudo -u postgres psql -c "CREATE USER karaokemugen_app WITH ENCRYPTED PASSWORD 'musubi';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE karaokemugen_app TO karaokemugen_app;"
sudo -u postgres psql -c "CREATE EXTENSION unaccent;" -d karaokemugen_app
service postgresql stop

# set the config file

cp -f ${MUGEN_BASE_DIR}/config.sample.yml ${MUGEN_BASE_DIR}/config.yml
sed -i 's/FullScreen: false/FullScreen: true/' ${MUGEN_BASE_DIR}/config.yml
sed -i 's/Stats: undefined/Stats: true/' ${MUGEN_BASE_DIR}/config.yml
sed -i 's/Message: null/Message: ./' ${MUGEN_BASE_DIR}/config.yml
sed -i '0,/Enabled: true/! s/Enabled: true/Enabled: false/' ${MUGEN_BASE_DIR}/config.yml

# fix rights

chown -R pi: ${MUGEN_BASE_DIR}
