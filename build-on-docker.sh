#!/bin/bash -e

cd ./pi-gen
cp -rf ../files/04-install-karaoke-mugen ./stage2/
echo 'IMG_NAME=KaraokeMugen
CONTINUE=1
PRESERVE_CONTAINER=1
ENABLE_SSH=1
STAGE_LIST="stage0 stage1 stage2"' > config
./build-docker.sh
cd -
