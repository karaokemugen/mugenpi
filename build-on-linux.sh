#!/bin/bash -e

if [[ "$(id -u)" != "0" ]]; then
	echo "Please run as root" 1>&2
	exit 1
fi

apt-get update
apt-get install -y coreutils quilt parted qemu-user-static debootstrap zerofree zip dosfstools bsdtar libcap2-bin grep rsync xz-utils file git curl

cd ./pi-gen
cp -rf ../files/04-install-karaoke-mugen ./stage2/
IMG_NAME=KaraokeMugen ENABLE_SSH=1 STAGE_LIST="stage0 stage1 stage2" ./build.sh
cd -
